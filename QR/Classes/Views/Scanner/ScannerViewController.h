//
//  ScannerViewController.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ScannerViewController : BaseViewController<AVCaptureMetadataOutputObjectsDelegate>

@end
