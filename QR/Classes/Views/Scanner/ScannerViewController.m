//
//  ScannerViewController.m
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "ScannerViewController.h"

#import "WrapperBeanFactory.h"

@interface ScannerViewController ()
@property (strong, nonatomic) AVCaptureDevice* device;
@property (strong, nonatomic) AVCaptureDeviceInput* input;
@property (strong, nonatomic) AVCaptureMetadataOutput* output;
@property (strong, nonatomic) AVCaptureSession* session;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer* preview;

@property (assign, nonatomic) BOOL touchToFocusEnabled;
@end

@implementation ScannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    if(![self isCameraAvailable]) {
        [UIAlertView showMessage:@"อุปกรณ์ไม่รองรับ!"];
    }else{
        [self setupScanner];
        [self startScanning];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [self stopScanning];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)evt
{
    if(self.touchToFocusEnabled) {
        UITouch *touch=[touches anyObject];
        CGPoint pt = [touch locationInView:self.view];
        [self focus:pt];
    }
}

#pragma mark - Mask

- (void)overlayMask{
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.view.bounds];
    
    int width = CGRectGetWidth(self.view.bounds);
    CGRect transparentFrame = CGRectMake(0, (CGRectGetHeight(self.view.bounds)-width)/2, width, width);
    UIBezierPath *transparentPath = [UIBezierPath bezierPathWithRect:transparentFrame];
    overlayPath.usesEvenOddFillRule = true;
    [overlayPath appendPath:transparentPath];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [[UIColor blackColor] colorWithAlphaComponent:0.7].CGColor;
    [self.view.layer insertSublayer:fillLayer atIndex:1];
}

#pragma mark -
#pragma mark AVFoundationSetup

- (void) setupScanner{
    if (self.session) {
        return;
    }
    self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    self.input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    self.session = [[AVCaptureSession alloc] init];
    self.output = [[AVCaptureMetadataOutput alloc] init];
    [self.session addOutput:self.output];
    [self.session addInput:self.input];
    
    [self.output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    self.output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    self.preview = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    [self.preview setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.preview setFrame:self.view.bounds];
    [self.view.layer insertSublayer:self.preview atIndex:0];
    [self overlayMask];
}

#pragma mark -
#pragma mark Helper Methods

- (BOOL) isCameraAvailable{
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    return [videoDevices count] > 0;
}

- (void)startScanning{
    [self.session startRunning];
}

- (void) stopScanning{
    [self.session stopRunning];
}

- (void) setTorch:(BOOL) aStatus{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [device lockForConfiguration:nil];
    if ( [device hasTorch] ) {
        if ( aStatus ) {
            [device setTorchMode:AVCaptureTorchModeOn];
        } else {
            [device setTorchMode:AVCaptureTorchModeOff];
        }
    }
    [device unlockForConfiguration];
}

- (void)focus:(CGPoint) aPoint{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if([device isFocusPointOfInterestSupported] &&
       [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        double screenWidth = screenRect.size.width;
        double screenHeight = screenRect.size.height;
        double focus_x = aPoint.x/screenWidth;
        double focus_y = aPoint.y/screenHeight;
        if([device lockForConfiguration:nil]) {
//            if([self.delegate respondsToSelector:@selector(scannerController:didTapToFocusOnPoint:)]) {
//                [self.delegate scannerController:self didTapToFocusOnPoint:aPoint];
//            }
            [device setFocusPointOfInterest:CGPointMake(focus_x,focus_y)];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            if ([device isExposureModeSupported:AVCaptureExposureModeAutoExpose]){
                [device setExposureMode:AVCaptureExposureModeAutoExpose];
            }
            [device unlockForConfiguration];
        }
    }
}

#pragma mark -
#pragma mark AVCaptureMetadataOutputObjectsDelegate

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            NSString *value = [metadataObj stringValue];
            [UIAlertView showMessage:value];
            // sending qrcode value
            [self stopScanning];
        }
    }
}

#pragma mark - Action

- (IBAction)onHistoryPressed:(id)sender {
    id viewController = [[WrapperBeanFactory sharedInstance] getObject:@"historyViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}

@end
