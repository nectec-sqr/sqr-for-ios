//
//  HistoryViewCell.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HighlightViewCell.h"

@class QR;

@interface HistoryViewCell : HighlightViewCell

- (void)dislay:(QR *)qr;
+ (float)heighForItem:(QR *)qr screenSize:(CGSize)size;

@end
