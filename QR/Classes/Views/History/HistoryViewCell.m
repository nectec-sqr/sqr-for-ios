//
//  HistoryViewCell.m
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "HistoryViewCell.h"

#import "UIImageView+WebCache.h"

#import "QR.h"

@interface HistoryViewCell()
@property (nonatomic) UIImageView *thumbnailView;
@property (nonatomic) UIImageView *iconView;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *autorLabel;
@property (nonatomic) UILabel *dateLabel;
@property (nonatomic) UIButton *downloadButton;

@property (nonatomic) QR *previewObj;
@end

@implementation HistoryViewCell

- (instancetype)init{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)initUI{
    self.backgroundColor = [UIColor whiteColor];
    
    _thumbnailView = [[UIImageView alloc] init];
    _thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
    _thumbnailView.clipsToBounds = YES;
    _thumbnailView.backgroundColor = [UIColor themeColor];
    [self addSubview:_thumbnailView];
    
    _iconView = [[UIImageView alloc] init];
    [self addSubview:_iconView];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont boldSystemFontOfSize:16];
    //_titleLabel.backgroundColor = [UIColor greenColor];
    _titleLabel.numberOfLines = 2;
    [self addSubview:_titleLabel];
    
    _autorLabel = [[UILabel alloc] init];
    _autorLabel.font = [UIFont systemFontOfSize:13];
    _autorLabel.textColor = RGBCOLOR(140, 140, 140);
    //_autorLabel.backgroundColor = [UIColor orangeColor];
    [self addSubview:_autorLabel];
    
    _dateLabel = [[UILabel alloc] init];
    _dateLabel.font = [UIFont systemFontOfSize:13];
    _dateLabel.textColor = RGBCOLOR(140, 140, 140);
    //_dateLabel.backgroundColor = [UIColor redColor];
    [self addSubview:_dateLabel];
    
    _downloadButton = [[UIButton alloc] init];
    _downloadButton.backgroundColor = [UIColor lightGrayColor];
    _downloadButton.titleLabel.font = [UIFont systemFontOfSize:10];
    [_downloadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_downloadButton setImage:[UIImage imageNamed:@"icon_download"] forState:UIControlStateNormal];
    [_downloadButton setTitle:@"OFFLINE" forState:UIControlStateNormal];
    [self addSubview:_downloadButton];
}

- (void)dislay:(QR *)qr{
    if(qr){
        _previewObj = qr;
        
        switch (qr.type) {
            case kQRContentTypeBook:
                _iconView.image = [UIImage imageNamed:@"icon_book"];
                break;
            case kQRContentTypeExternalLink:
                _iconView.image = [UIImage imageNamed:@"icon_link"];
                break;
            case kQRContentTypeVideo:
                _iconView.image = [UIImage imageNamed:@"icon_video"];
                break;
            default:
                break;
        }
        
        switch (qr.offline) {
            case kQROfflineContentTypeNotAvailable:
                _downloadButton.hidden = YES;
                break;
            case kQROfflineContentTypeAvailable:
                _downloadButton.backgroundColor = [UIColor themeColor];
                [_downloadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [_downloadButton setImage:[UIImage imageNamed:@"icon_download"] forState:UIControlStateNormal];
                break;
            case kQROfflineContentTypeKeeped:
                _downloadButton.backgroundColor = [UIColor whiteColor];
                [_downloadButton setTitle:@"KEEPED" forState:UIControlStateNormal];
                [_downloadButton setTitleColor:[UIColor themeColor] forState:UIControlStateNormal];
                [_downloadButton setImage:[UIImage imageNamed:@"icon_download_theme"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
        
        _titleLabel.text = qr.title;
        _autorLabel.text = qr.author;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"เมื่อวันที่ dd MMMM yyyy เวลา hh:mm:ss"];
        _dateLabel.text = [formatter stringFromDate:qr.publishDate];
        
        if (qr.thumbnailURL) {
            [_thumbnailView sd_setImageWithURL:qr.thumbnailURL];
        }
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    int padding = 10;
    int width = CGRectGetWidth(self.frame);
    int left = 0;
    
    if (_previewObj.thumbnailURL) {
        _thumbnailView.frame = CGRectMake(0, 0, 110, 90);
        left = 110;
    }else{
        _thumbnailView.frame = CGRectZero;
    }
    
    _iconView.frame = CGRectMake(left + padding, 10, 24, 24);
    
    _titleLabel.frame = CGRectMake(CGRectGetMaxX(_iconView.frame) + 5, 13, width - (CGRectGetMaxX(_iconView.frame) + padding*2), 50);
    [_titleLabel sizeToFit];
    
    _autorLabel.frame = CGRectMake(left + padding, MAX(CGRectGetMaxY(_titleLabel.frame) + 5, CGRectGetMaxY(_iconView.frame)), width - padding*2, 20);
    
    _dateLabel.frame = CGRectMake(left + padding, CGRectGetMaxY(_autorLabel.frame), width - padding*2, 20);
    
    _downloadButton.frame = CGRectMake(width - (84 + padding), CGRectGetHeight(self.frame) - (28 + padding) , 84, 28);
    _downloadButton.layer.cornerRadius = 14;
}

+ (float)heighForItem:(QR *)qr screenSize:(CGSize)size{
    if (qr && qr.thumbnailURL && qr.offline != kQROfflineContentTypeNotAvailable) {
        return 140;
    }
    return 110;
}

@end
