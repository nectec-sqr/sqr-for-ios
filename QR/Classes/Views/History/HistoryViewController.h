//
//  HistoryViewController.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "BaseViewController.h"

@interface HistoryViewController : BaseViewController

@property (nonatomic) NSArray *qrs;

@end
