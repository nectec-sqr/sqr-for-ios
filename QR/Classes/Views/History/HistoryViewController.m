//
//  HistoryViewController.m
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryViewCell.h"

#import "QR.h"

#define CELL_INDENTIFIER        @"HistoryViewCell"

@interface HistoryViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self commonInit];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI{
    [self setupCloseButton];
    _collectionView.backgroundColor = [UIColor backgroundColor];
    [_collectionView registerClass:[HistoryViewCell class] forCellWithReuseIdentifier:CELL_INDENTIFIER];
}

- (void)commonInit{
    _qrs = [QR mockups];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

#pragma mark - UICollectionView
 
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _qrs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HistoryViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_INDENTIFIER forIndexPath:indexPath];
    [cell dislay:_qrs[indexPath.row]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float height = [HistoryViewCell heighForItem:_qrs[indexPath.row] screenSize:_collectionView.frame.size];
    return CGSizeMake(CGRectGetWidth(_collectionView.frame), height);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

@end
