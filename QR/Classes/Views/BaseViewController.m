//
//  BaseViewController.m
//  TVIS
//
//  Created by Kiettiphong Manovisut on 3/18/15.
//  Copyright (c) 2015 SPTDEV. All rights reserved.
//

#import "BaseViewController.h"

#import "MBProgressHUD.h"

@interface BaseViewController()
@property (nonatomic) UIScrollView *scrollView;
@end

@implementation BaseViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self initBaseUI];
    [self setupExtendedLayout];
}

- (void)initBaseUI{
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor = [UIColor backgroundColor];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Utils

- (void)setupExtendedLayout{
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (UIActivityIndicatorView *)setupActivityIndicator{
    UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [activityIndicator setHidesWhenStopped:YES];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    return activityIndicator;
}

#pragma mark - Keyboard

- (void)registerForKeyboardNotifications:(UIScrollView *)scrollView{
    _scrollView = scrollView;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    if (_scrollView) {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + 20, 0.0);
        _scrollView.contentInset = contentInsets;
        _scrollView.scrollIndicatorInsets = contentInsets;
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    if (_scrollView) {
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        _scrollView.contentInset = contentInsets;
        _scrollView.scrollIndicatorInsets = contentInsets;
    }
}

#pragma mark - Interface

- (void)setupNextButton{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"ถัดไป", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onNextPressed)];
}

- (void)setupCloseButton{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"ยกเลิก", nil) style:UIBarButtonItemStylePlain target:self action:@selector(onClosePressed)];
}

#pragma mark - Loading

- (void)showLoading{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.fillBackground = YES;
    hud.mode = MBProgressHUDModeText;
    hud.labelText = NSLocalizedString(@"กำลังโหลด...", nil);
}

- (void)hideLoading{
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
}

#pragma mark - Inherit

- (void)onNextPressed{
    
}

- (void)onClosePressed{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
