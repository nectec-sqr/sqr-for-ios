//
//  BaseViewController.h
//  TVIS
//
//  Created by Kiettiphong Manovisut on 3/18/15.
//  Copyright (c) 2015 SPTDEV. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MultiSelectorViewController;

@interface BaseViewController : UIViewController<UIPopoverControllerDelegate>

- (void)registerForKeyboardNotifications:(UIScrollView *)scrollView;
- (void)keyboardWasShown:(NSNotification*)aNotification;
- (void)keyboardWillBeHidden:(NSNotification*)aNotification;

- (UIActivityIndicatorView *)setupActivityIndicator;
- (void)showLoading;
- (void)hideLoading;

- (void)setupNextButton;
- (void)setupCloseButton;
- (void)onNextPressed;
- (void)onClosePressed;

@end
