//
//  NSString+QR.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (QR)

+ (NSString *)apiURL;

@end
