//
//  UIColor+QR.m
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "UIColor+QR.h"

@implementation UIColor (QR)

+ (UIColor *)backgroundColor{
    return RGBCOLOR(240, 240, 240);
}

+ (UIColor *)themeColor{
    return RGBCOLOR(84, 151, 204);
}

@end
