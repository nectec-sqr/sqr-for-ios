//
//  UIColor+QR.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (QR)

+ (UIColor *)backgroundColor;
+ (UIColor *)themeColor;

@end
