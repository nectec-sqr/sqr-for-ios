//
//  BhDialogProgressIndicator.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/23/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BhDialogProgressIndicator : NSObject

@property (nonatomic, strong) UIView *parentView;
@property (nonatomic, strong) NSString *labelText;
@property (nonatomic, strong) NSString *detailsLabelText;

+ (BhDialogProgressIndicator *)progressWithView:(UIView *)view;

+ (BhDialogProgressIndicator *)progressWithView:(UIView *)view
                                      labelText:(NSString *)labelText
                               detailsLabelText:(NSString *)detailsLabelText;

- (id)initWithView:(UIView *)view;
- (id)initWithView:(UIView *)view
         labelText:(NSString *)labelText
  detailsLabelText:(NSString *)detailsLabelText;

- (void)start;
- (void)stop;

@end
