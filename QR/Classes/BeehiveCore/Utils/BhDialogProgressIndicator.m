//
//  BhDialogProgressIndicator.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/23/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "BhDialogProgressIndicator.h"

#import "MBProgressHUD.h"

@interface BhDialogProgressIndicator()
@property MBProgressHUD *progressHUD;
@end

@implementation BhDialogProgressIndicator

+ (BhDialogProgressIndicator *)progressWithView:(UIView *)view {
    if (!view) return nil;
    return [[BhDialogProgressIndicator alloc] initWithView:view];
}

+ (BhDialogProgressIndicator *)progressWithView:(UIView *)view labelText:(NSString *)labelText detailsLabelText:(NSString *)detailsLabelText{
    BhDialogProgressIndicator *indicator = [[BhDialogProgressIndicator alloc] initWithView:view labelText:labelText detailsLabelText:detailsLabelText];
    return indicator;
}

- (id)initWithView:(UIView *)view {
    return [self initWithView:view labelText:nil detailsLabelText:nil];
}

- (id)initWithView:(UIView *)view
         labelText:(NSString *)labelText
  detailsLabelText:(NSString *)detailsLabelText {
    self = [super init];
    
    if (self) {
        _parentView = view;
        _labelText = labelText;
        _detailsLabelText = detailsLabelText;
    }
    
    return self;
    
}

- (void)start {
    _progressHUD = [MBProgressHUD showHUDAddedTo:_parentView animated:YES];
    _progressHUD.mode = MBProgressHUDModeIndeterminate;
    _progressHUD.labelText = (_labelText)? _labelText : NSLocalizedString(@"Loading", @"Loading");
    if (_detailsLabelText && [_detailsLabelText length] > 0) {
        _progressHUD.detailsLabelText = _detailsLabelText;
    }
}

- (void)stop {
    [_progressHUD hide:NO];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    [_progressHUD removeFromSuperview];
    _progressHUD = nil;
}

- (void)hudWasHidden {
    [_progressHUD removeFromSuperview];
    _progressHUD = nil;
}

@end
