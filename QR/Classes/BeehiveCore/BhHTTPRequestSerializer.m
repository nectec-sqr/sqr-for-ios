//
//  BhHTTPRequestSerializer.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/7/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "BhHTTPRequestSerializer.h"

#import "AFURLRequestSerialization.h"

@implementation BhHTTPRequestSerializer

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(id)parameters
                                     error:(NSError *__autoreleasing *)error{
    return [_serializer requestWithMethod:method URLString:URLString parameters:parameters error:error];
}

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withPhotoUploads:(NSArray *)photosUpload
                                                   name:(NSString *)name
                                               fileName:(NSString *)fileName
                                               mimeType:(NSString *)mimeType
                                                  error:(NSError *__autoreleasing *)error{
    
    void (^constructingBodyWithBlock)(id <AFMultipartFormData>) = ^(id<AFMultipartFormData> formData) {
        if (photosUpload) {
            for (NSData *data in photosUpload) {
                if (data) {
                    [formData appendPartWithFileData:data
                                                name:name
                                            fileName:fileName
                                            mimeType:mimeType];
                }
            }
        }
    };
    
    return [_serializer multipartFormRequestWithMethod:method URLString:URLString parameters:parameters constructingBodyWithBlock:constructingBodyWithBlock error:error];
}

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withVideoUploads:(NSArray *)videosUpload
                                                   name:(NSString *)name
                                               fileName:(NSString *)fileName
                                               mimeType:(NSString *)mimeType
                                                  error:(NSError *__autoreleasing *)error{
    void (^constructingBodyWithBlock)(id <AFMultipartFormData>) = ^(id<AFMultipartFormData> formData) {
        if (videosUpload) {
            for (NSData *data in videosUpload) {
                if (data) {
                    [formData appendPartWithFileData:data
                                                name:name
                                            fileName:fileName
                                            mimeType:mimeType];
                }
            }
        }
    };
    
    return [_serializer multipartFormRequestWithMethod:method URLString:URLString parameters:parameters constructingBodyWithBlock:constructingBodyWithBlock error:error];
}

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withPhotoUploads:(NSArray *)photosUpload
                                             photosName:(NSString *)photosName
                                         photosFileName:(NSString *)photosFileName
                                         photosMimeType:(NSString *)photosMimeType
                                           videosUpload:(NSArray *)videosUpload
                                             videosName:(NSString *)videosName
                                         videosFileName:(NSString *)videosFileName
                                         videosMimeType:(NSString *)videosMimeType
                                                  error:(NSError *__autoreleasing *)error{
    
    void (^constructingBodyWithBlock)(id <AFMultipartFormData>) = ^(id<AFMultipartFormData> formData) {
        if (photosUpload) {
            for (int i = 0; i < photosUpload.count; i++) {
                NSData *data = photosUpload[i];
                if (data) {
                    NSString *name = [NSString stringWithFormat:@"%@[%d]", photosName, i];
                    [formData appendPartWithFileData:data
                                                name:name
                                            fileName:photosFileName
                                            mimeType:photosMimeType];
                }
            }
        }
        
        if (videosUpload) {
            for (int i = 0; i < videosUpload.count; i++) {
                NSData *data = videosUpload[i];
                if (data) {
                    NSString *name = [NSString stringWithFormat:@"%@[%d]", videosName, i];
                    [formData appendPartWithFileData:data
                                                name:name
                                            fileName:videosFileName
                                            mimeType:videosMimeType];
                }
            }
        }
    };
    
    return [_serializer multipartFormRequestWithMethod:method URLString:URLString parameters:parameters constructingBodyWithBlock:constructingBodyWithBlock error:error];

}

@end
