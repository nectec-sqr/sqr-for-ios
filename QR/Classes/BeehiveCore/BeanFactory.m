//
//  BeanFactory.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/15/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "BeanFactory.h"

@interface BeanFactory()
@property (nonatomic, strong) NSMutableDictionary *prototypeCreatorMethodDictionary;
@property (nonatomic, strong) NSMutableDictionary *singletonDictionary;
@property (nonatomic, strong) NSMutableSet *singletonSet;
@end

@implementation BeanFactory

- (instancetype)init{
    self = [super init];
    if (self) {
        _prototypeCreatorMethodDictionary = [[NSMutableDictionary alloc] init];
        _singletonDictionary = [[NSMutableDictionary alloc] init];
        _singletonSet = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)initSingletons{
    for (NSString *key in _singletonSet) {
        id singleton = [self newObject:key];
        _singletonDictionary[key] = singleton;
    }
}

- (void)addPrototypeCreatorMethod:(NSString *)name{
    [_prototypeCreatorMethodDictionary setValue:name forKey:name];
}

- (id)newObject:(NSString *)beanName{
    NSString *creatorMethod = _prototypeCreatorMethodDictionary[beanName];
    if (creatorMethod) {
        SEL selector = NSSelectorFromString(creatorMethod);
        if (selector) {
            if ([self respondsToSelector:selector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                return [self performSelector:selector];
#pragma clang diagnostic pop
            }
        }
    }
    return nil;
}

- (id)getObject:(NSString *)beanName{
    id bean = [self newObject:beanName];
    return bean;
}

@end
