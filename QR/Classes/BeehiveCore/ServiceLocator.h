//
//  ServiceLocator.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/7/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceLocator : NSObject

+ (instancetype)sharedInstance;

- (void)registerLocator:(ServiceLocator *)locator;
- (id)getService:(NSString *)serviceName;
@end
