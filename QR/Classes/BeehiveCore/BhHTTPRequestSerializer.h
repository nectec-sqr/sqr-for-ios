//
//  BhHTTPRequestSerializer.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/7/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPRequestSerializer;

@interface BhHTTPRequestSerializer : NSObject

@property (nonatomic, strong) AFHTTPRequestSerializer *serializer;

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(id)parameters
                                     error:(NSError *__autoreleasing *)error;

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withPhotoUploads:(NSArray *)photosUpload
                                                   name:(NSString *)name
                                               fileName:(NSString *)fileName
                                               mimeType:(NSString *)mimeType
                                                  error:(NSError *__autoreleasing *)error;

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withVideoUploads:(NSArray *)videosUpload
                                                   name:(NSString *)name
                                               fileName:(NSString *)fileName
                                               mimeType:(NSString *)mimeType
                                                  error:(NSError *__autoreleasing *)error;

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withPhotoUploads:(NSArray *)photosUpload
                                             photosName:(NSString *)photosName
                                         photosFileName:(NSString *)photosFileName
                                         photosMimeType:(NSString *)photosMimeType
                                           videosUpload:(NSArray *)videosUpload
                                             videosName:(NSString *)videosName
                                         videosFileName:(NSString *)videosFileName
                                         videosMimeType:(NSString *)videosMimeType
                                                  error:(NSError *__autoreleasing *)error;

@end
