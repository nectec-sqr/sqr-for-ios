//
//  BhHTTPRequestOperationManager.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 10/16/14.
//  Copyright (c) 2014 SpeechDeveloper. All rights reserved.
//

#import "BhHTTPRequestOperationManager.h"

#import "BhHTTPRequestSerializer.h"

@implementation BhHTTPRequestOperationManager

- (instancetype)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:url];
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        securityPolicy.allowInvalidCertificates = YES;
        self.securityPolicy = securityPolicy;
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
    }
    return self;
}

@end
