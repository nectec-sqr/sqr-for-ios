//
//  WrapperBeanFactory.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/15/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "WrapperBeanFactory.h"

static WrapperBeanFactory *instance = nil;
static dispatch_once_t pred;

@implementation WrapperBeanFactory

+ (instancetype)sharedInstance{
    dispatch_once(&pred, ^{
        if (!instance) {
            instance = [[[self class] alloc] init];
        }
    });
    return instance;
}

- (id)newObject:(NSString *)name{
    return [_beanFactory newObject:name];
}

- (id)getObject:(NSString *)name{
    return [_beanFactory getObject:name];
}

@end
