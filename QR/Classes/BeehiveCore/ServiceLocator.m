//
//  ServiceLocator.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/7/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "ServiceLocator.h"

static ServiceLocator *instance = nil;
static dispatch_once_t pred; // Fast initial once time singleton https://mikeash.com/pyblog/friday-qa-2009-10-02-care-and-feeding-of-singletons.html


@interface ServiceLocator()
@property (nonatomic) ServiceLocator *serviceLocator;
@end

@implementation ServiceLocator

+ (instancetype)sharedInstance {
    dispatch_once(&pred, ^{
        if (instance == nil) {
            instance = [[[self class] alloc] init];
        }
    });
    
    return instance;
}

- (void)registerLocator:(ServiceLocator *)locator {
    self.serviceLocator = locator;
}

- (id)getService:(NSString *)serviceName {
    return [_serviceLocator getService:serviceName];
}

@end
