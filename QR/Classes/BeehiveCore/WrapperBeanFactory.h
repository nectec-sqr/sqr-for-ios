//
//  WrapperBeanFactory.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/15/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BeanFactory.h"

@interface WrapperBeanFactory : NSObject{
    BeanFactory *_beanFactory;
}

@property (strong, nonatomic) BeanFactory *beanFactory;

+ (instancetype)sharedInstance;
- (id)getObject:(NSString *)name;
- (id)newObject:(NSString *)name;

@end
