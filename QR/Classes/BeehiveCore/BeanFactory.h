//
//  BeanFactory.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/15/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeanFactory : NSObject

- (id)newObject:(NSString *)beanName;
- (id)getObject:(NSString *)beanName;

- (void)addPrototypeCreatorMethod:(NSString *)name;

@end
