//
//  User.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/9/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject <NSCoding>

@property (nonatomic) NSString *Id;
@property (nonatomic) NSString *username;
@property (nonatomic) NSString *firstname;
@property (nonatomic) NSString *lastname;
@property (nonatomic) NSString *mobile;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *imageURL;
@property (nonatomic) NSString *thumbnailURL;
@property (nonatomic) NSString *trsNumber;

- (instancetype)initWithDictionary:(NSDictionary *)jsonObject;

@end
