//
//  QR.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QR : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *author;
@property (nonatomic) NSURL *thumbnailURL;
@property (nonatomic) NSDate *publishDate;
@property (nonatomic) QRContentType type;
@property (nonatomic) QROfflineContentType offline;

+ (NSArray *)mockups;

@end
