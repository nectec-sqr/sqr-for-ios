//
//  User.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/9/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "User.h"

#define FIELD_MEMBER_ID         @"member_id"
#define FIELD_USERNAME          @"username"
#define FIELD_EMAIL             @"email"
#define FIELD_FIRSTNAME         @"firstname"
#define FIELD_LASTNAME          @"lastname"
#define FIELD_MOBILE            @"mobile"
#define FIELD_IMAGE             @"image"
#define FIELD_IMAGE_THUMBNAIL   @"thumbnail"
#define FIELD_TRS_ID            @"trsnumber"

@implementation User

- (instancetype)initWithDictionary:(NSDictionary *)jsonObject{
    if (jsonObject) {
        self = [super init];
        if (self) {
            self.Id = [jsonObject valueForKey:FIELD_MEMBER_ID];
            self.username = [jsonObject valueForKey:FIELD_USERNAME];
            self.email = [jsonObject valueForKey:FIELD_EMAIL];
            self.firstname = [jsonObject valueForKey:FIELD_FIRSTNAME];
            self.lastname = [jsonObject valueForKey:FIELD_LASTNAME];
            self.mobile = [jsonObject valueForKey:FIELD_MOBILE];
            self.imageURL = [jsonObject valueForKey:FIELD_IMAGE];
            self.thumbnailURL = [jsonObject valueForKey:FIELD_IMAGE_THUMBNAIL];
            self.trsNumber = [jsonObject valueForKey:FIELD_TRS_ID];
        }
        return self;
    }
    return nil;
}


- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.Id forKey:FIELD_MEMBER_ID];
    [encoder encodeObject:self.username forKey:FIELD_USERNAME];
    [encoder encodeObject:self.email forKey:FIELD_EMAIL];
    [encoder encodeObject:self.firstname forKey:FIELD_FIRSTNAME];
    [encoder encodeObject:self.lastname forKey:FIELD_LASTNAME];
    [encoder encodeObject:self.mobile forKey:FIELD_MOBILE];
    [encoder encodeObject:self.imageURL forKey:FIELD_IMAGE];
    [encoder encodeObject:self.thumbnailURL forKey:FIELD_IMAGE_THUMBNAIL];
    [encoder encodeObject:self.trsNumber forKey:FIELD_TRS_ID];
}

- (id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if( self != nil ){
        self.Id = [decoder decodeObjectForKey:FIELD_MEMBER_ID];
        self.username = [decoder decodeObjectForKey:FIELD_USERNAME];
        self.email = [decoder decodeObjectForKey:FIELD_EMAIL];
        self.firstname = [decoder decodeObjectForKey:FIELD_FIRSTNAME];
        self.lastname = [decoder decodeObjectForKey:FIELD_LASTNAME];
        self.mobile = [decoder decodeObjectForKey:FIELD_MOBILE];
        self.imageURL = [decoder decodeObjectForKey:FIELD_IMAGE];
        self.thumbnailURL = [decoder decodeObjectForKey:FIELD_IMAGE_THUMBNAIL];
        self.trsNumber = [decoder decodeObjectForKey:FIELD_TRS_ID];
    }
    return self;
}

@end
