//
//  QR.m
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "QR.h"

@implementation QR

+ (NSArray *)mockups{
    QR *qr1 = [[QR alloc] init];
    qr1.title = @"หนังสือเสริมความรู้อิงหลักสูตร วิทยาศาสตร์ ป.1-ป.6";
    qr1.author = @"หนังสือเรียน อจท.";
    qr1.publishDate = [NSDate date];
    qr1.type = kQRContentTypeBook;
    qr1.offline = kQROfflineContentTypeNotAvailable;
    
    QR *qr6 = [[QR alloc] init];
    qr6.title = @"หนังสือเสริมความรู้อิงหลักสูตร วิทยาศาสตร์";
    qr6.author = @"หนังสือเรียน อจท.";
    qr6.publishDate = [NSDate date];
    qr6.type = kQRContentTypeExternalLink;
    qr6.offline = kQROfflineContentTypeNotAvailable;
    
    QR *qr2 = [[QR alloc] init];
    qr2.title = @"หนังสือเรียนแม่บทมาตรฐาน หลักสูตรแกนกลางฯ NEW AHA! ENGLISH ป.1 ";
    qr2.author = @"New Aha English";
    qr2.publishDate = [NSDate date];
    qr2.type = kQRContentTypeBook;
    qr2.offline = kQROfflineContentTypeKeeped;
    
    QR *qr3 = [[QR alloc] init];
    qr3.title = @"ประกาศ คำสั่ง คณะรักษาความสงบแห่งชาติ (คสช.) National Council for Peace and Order.";
    qr3.author = @"คณะรักษาความสงบแห่งชาติ (คสช.)";
    qr3.publishDate = [NSDate date];
    qr3.type = kQRContentTypeExternalLink;
    qr3.offline = kQROfflineContentTypeNotAvailable;
    
    QR *qr4 = [[QR alloc] init];
    qr4.title = @"คลิป MU [by Mahidol] แกงไทยต้านมะเร็ง";
    qr4.author = @"Mahidol Channel มหิดล แชนแนล";
    qr4.publishDate = [NSDate date];
    qr4.thumbnailURL = [NSURL URLWithString:@"http://www.pixeden.com/media/k2/galleries/542/001-gravity-A4-paper-presentation-mockup-brand-psd.jpg"];
    qr4.type = kQRContentTypeVideo;
    qr4.offline = kQROfflineContentTypeAvailable;
    
    QR *qr5 = [[QR alloc] init];
    qr5.title = @"Taylor Swift - New Romantics";
    qr5.author = @"Taylor Swift";
    qr5.publishDate = [NSDate date];
    qr5.thumbnailURL = [NSURL URLWithString:@"https://d13yacurqjgara.cloudfront.net/users/145669/screenshots/2284139/a4-z-fold-mock-up-dribble.jpg"];
    qr5.type = kQRContentTypeVideo;
    qr5.offline = kQROfflineContentTypeNotAvailable;
    
    return @[qr1, qr2, qr3, qr4, qr6, qr5];
}

@end
