//
//  UserDefault.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/14/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "UserDefault.h"

#define FIELD_INITIAL       @"field_initial"
#define FIELD_AGREEMENT     @"field_agreement"
#define FIELD_SYNCHRONIZE   @"field_is_synchronize"

#define FIELD_USER          @"field_user"
#define FIELD_DEVICE_TOKEN  @"field_device_token"

static UserDefault *instance = nil;
static dispatch_once_t pred;

@implementation UserDefault

+ (UserDefault *)sharedInstance{
    dispatch_once(&pred, ^{
        if (!instance) {
            instance = [[[self class] alloc] init];
            [instance loadDefaults];
        }
    });
    return instance;
}

#pragma mark - NSUserDefaults

- (void)setUserDefaultsWithObject:(id)object forKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)userDefaultsObjectForKey:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

#pragma mark - UserDefault

- (void)loadDefaults{
    NSData *userAsData = [self userDefaultsObjectForKey:FIELD_USER];
    if (userAsData) {
        _user = [NSKeyedUnarchiver unarchiveObjectWithData:userAsData];
        _registered = (self.user != nil);
    }
    _initial = [[NSUserDefaults standardUserDefaults] boolForKey:FIELD_INITIAL];
    _synchronize = [[NSUserDefaults standardUserDefaults] boolForKey:FIELD_SYNCHRONIZE];
    _agreement = [[NSUserDefaults standardUserDefaults] boolForKey:FIELD_AGREEMENT];
}

#pragma mark - UserDefault Setter

- (void)setInitial:(BOOL)isInitial{
    _initial = isInitial;
    [[NSUserDefaults standardUserDefaults] setBool:isInitial forKey:FIELD_INITIAL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setAgreement:(BOOL)agreement{
    _agreement = agreement;
    [[NSUserDefaults standardUserDefaults] setBool:agreement forKey:FIELD_AGREEMENT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setSynchronize:(BOOL)isSynchronize{
    _synchronize = isSynchronize;
    [[NSUserDefaults standardUserDefaults] setBool:isSynchronize forKey:FIELD_SYNCHRONIZE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setUser:(User *)user{
    if (_user != user) {
        _user = user;
        NSData *userAsData = [NSKeyedArchiver archivedDataWithRootObject:user];
        [self setUserDefaultsWithObject:userAsData forKey:FIELD_USER];
        _registered = YES;
    }
}

- (void)setDeviceToken:(NSString *)deviceToken{
    if (_deviceToken != deviceToken) {
        _deviceToken = deviceToken;
        [self setUserDefaultsWithObject:_deviceToken forKey:FIELD_DEVICE_TOKEN];
    }
}

#pragma mark - UserDefault Getter

- (void)loadUser{
    NSData *userAsData = [self userDefaultsObjectForKey:FIELD_USER];
    if (userAsData) {
        _user = [NSKeyedUnarchiver unarchiveObjectWithData:userAsData];
        _registered = (self.user != nil);
    }
}

- (void)loadDeviceToken{
    NSString *token = [self userDefaultsObjectForKey:FIELD_DEVICE_TOKEN];
    if (token) {
        _deviceToken = token;
    }
}

- (void)invalidate {
    [self setUser:nil];
    _registered = NO;
    [self setAgreement:NO];
    [self setSynchronize:NO];
}

@end
