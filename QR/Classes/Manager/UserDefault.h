//
//  UserDefault.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/14/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface UserDefault : NSObject

@property (nonatomic) BOOL initial;
@property (nonatomic) BOOL agreement;
@property (nonatomic) BOOL registered;
@property (nonatomic) BOOL synchronize;
@property (nonatomic) User *user;

@property (nonatomic) NSString *deviceToken;

+ (UserDefault *)sharedInstance;

- (void)invalidate;

@end
