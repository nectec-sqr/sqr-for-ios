//
//  HighlightViewCell.m
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "HighlightViewCell.h"

@implementation HighlightViewCell

- (void)setHighlighted:(BOOL)highlighted{
    if (highlighted) {
        self.layer.backgroundColor = [[UIColor themeColor] colorWithAlphaComponent:0.3].CGColor;
    }else{
        [UIView animateWithDuration:0.25 animations:^{
            self.layer.backgroundColor = [UIColor whiteColor].CGColor;
        }];
    }
    [self setNeedsDisplay];
}

@end
