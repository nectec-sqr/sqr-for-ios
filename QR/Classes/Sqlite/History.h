//
//  DBManager.h
//  Readle for iOS
//
//  Created by Kiettiphong Manovisut on 1/24/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@class QR;

@interface History : NSObject{
    NSString *databasePath;
}

+ (History *)sharedInstance;

- (BOOL)inserts:(NSArray *)qrs;
- (BOOL)insert:(QR *)qr;
- (NSArray *)getObjects;
- (void)invalidate;

@end
