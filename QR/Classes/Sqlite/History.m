//
//  DBManager.m
//  Readle for iOS
//
//  Created by Kiettiphong Manovisut on 1/24/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//


#import "History.h"

#import "QR.h"

static History *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation History

+ (History *)sharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance createdb];
    }
    return sharedInstance;
}

/********************************************************************
 *                          Create Scheme
 *******************************************************************/

- (BOOL)createdb{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"amphurs.sqlite"]];
    NSLog(@"%@", databasePath);
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "create table if not exists amphurs(amphur_id text, amphur_name text, province_id int(11))";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK){
                isSuccess = NO;
                NSLog(@"Failed to create");
            }
            sqlite3_close(database);
            return  isSuccess;
        }
        else
        {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

/********************************************************************
 *                          Insert Scheme
 *******************************************************************/

- (BOOL)insert:(QR *)qr{
    return [self inserts:@[qr]];
}

- (BOOL)inserts:(NSArray *)qrs{
    BOOL status = NO;
//    const char *dbpath = [databasePath UTF8String];
//    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
//    {
//        if (!qrs) {
//            return NO;
//        }
//        for (QR *qr in qrs) {
//            NSString *insertSQL = [NSString stringWithFormat:@"insert or replace into amphurs (amphur_id, amphur_name, province_id) values (\"%@\", \"%@\", \"%@\")",
//                                   kv.value,
//                                   kv.key,
//                                   kv.provinceId];
//            
//            const char *insert_stmt = [insertSQL UTF8String];
//            sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
//            if (sqlite3_step(statement) == SQLITE_DONE){
//                status = YES;
//            }else{
//                NSLog(@"[SQL] Insert error. (%@, %s)", kv.key, sqlite3_errmsg(database));
//                status = NO;
//            }
//            sqlite3_reset(statement);
//        }
//    }else{
//        sqlite3_close(database);
//    }
    return status;
}



/********************************************************************
 *                          Select Scheme
 *******************************************************************/

- (NSArray *)getObjects{
//    const char *dbpath = [databasePath UTF8String];
//    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
//        NSString *querySQL = [NSString stringWithFormat:
//                              @"select * from amphurs where province_id like '%@' order by amphur_name asc", key];
//        const char *query_stmt = [querySQL UTF8String];
//        if (sqlite3_prepare_v2(database,
//                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
//        {
//            NSMutableArray *kvs = [[NSMutableArray alloc] initWithCapacity:0];
//            while (sqlite3_step(statement) == SQLITE_ROW)
//            {
//                NSString *amphur_id = [[NSString alloc] initWithUTF8String:
//                                       (const char *) sqlite3_column_text(statement, 0)];
//                NSString *amphur_name = [[NSString alloc] initWithUTF8String:
//                                         (const char *) sqlite3_column_text(statement, 1)];
//                NSString *province_id = [[NSString alloc] initWithUTF8String:
//                                         (const char *) sqlite3_column_text(statement, 2)];
//                
//                Amphur *kv = [[Amphur alloc] init];
//                kv.key = amphur_name;
//                kv.value = amphur_id;
//                kv.provinceId = province_id;
//                [kvs addObject:kv];
//            }
//            sqlite3_finalize(statement);
//            return kvs;
//        }
//    }else{
//        sqlite3_close(database);
//    }
    return nil;
}

/********************************************************************
 *                          Delete Scheme
 *******************************************************************/

- (void)invalidate{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *updateSQL = [NSString stringWithFormat:@"delete from amphurs"];
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(database, update_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE){
        }else{
            NSLog(@"[SQL] Can't delete %s", sqlite3_errmsg(database));
        }
        sqlite3_reset(statement);
    }else{
        sqlite3_close(database);
    }
}

@end
