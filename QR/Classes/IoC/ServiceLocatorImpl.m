//
//  ServiceLocatorImpl.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 10/16/14.
//  Copyright (c) 2014 SpeechDeveloper. All rights reserved.
//

#import "ServiceLocatorImpl.h"

#import "BhHTTPRequestOperationManager.h"
#import "BhHTTPRequestSerializer.h"

#import "APIClientImpl.h"

static ServiceLocatorImpl *instance = nil;

@interface ServiceLocatorImpl()
@property (nonatomic) APIClient *apiClient;
@end

@implementation ServiceLocatorImpl{
    NSMutableDictionary *_map;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self createServiceMap];
    }
    return self;
}

- (NSDictionary *)createServiceMap{
    _map = [[NSMutableDictionary alloc] init];
    [_map setObject:self.apiClient forKey:@"apiClient"];
    return _map;
}

- (id)getService:(NSString *)serviceName{
    return _map[serviceName];
}

- (APIClient *)apiClient{
    if (!_apiClient) {
        
        // Config AFNetworking
        AFHTTPRequestSerializer *afHTTPSerializer = [AFHTTPRequestSerializer serializer];
        AFJSONResponseSerializer *afJSONResponseSerializer = [AFJSONResponseSerializer serializer];
        AFHTTPRequestOperationManager *afHTTPRequestOperationManager = [AFHTTPRequestOperationManager manager];
        afHTTPRequestOperationManager.responseSerializer = afJSONResponseSerializer;
        
        // Config ApiClient Adapter
        BhHTTPRequestSerializer *serializer = [[BhHTTPRequestSerializer alloc] init];
        serializer.serializer = afHTTPSerializer;
        
        BhHTTPRequestOperationManager *requestOperationManager = [[BhHTTPRequestOperationManager alloc] init];
        //requestOperationManager.requestOperationManager = afHTTPRequestOperationManager;
        
        APIClientImpl *apiClient = [[APIClientImpl alloc] init];
        apiClient.operationManager = requestOperationManager;
        apiClient.serializer = serializer;
        
        _apiClient = apiClient;
    }
    return _apiClient;
}

//- (InformationService *)informationService{
//    if (!_informationService) {
//        _informationService = [[InformationService alloc] init];
//        _informationService.apiClient = _apiClient;
//    }
//    return _informationService;
//}

@end
