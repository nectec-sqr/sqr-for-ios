//
//  BeanFactoryImpl.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/15/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "BeanFactoryImpl.h"

#import "ServiceLocator.h"

@implementation BeanFactoryImpl

- (instancetype)init{
    self = [super init];
    if (self) {
        [self addPrototypeCreatorMethod:@"scannerViewController"];
        [self addPrototypeCreatorMethod:@"historyViewController"];
    }
    return self;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"

- (id)scannerViewController{
    id scannerViewController = [[NSClassFromString(@"ScannerViewController") alloc] initWithNibName:@"ScannerViewController" bundle:nil];
    return scannerViewController;
}

- (id)historyViewController{
    id historyViewController = [[NSClassFromString(@"HistoryViewController") alloc] initWithNibName:@"HistoryViewController" bundle:nil];
    NSString *title = @"ประวัติ";
    [historyViewController performSelector:@selector(setTitle:) withObject:title];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:historyViewController];
    return navigation;
}

#pragma clang diagnostic pop

@end
