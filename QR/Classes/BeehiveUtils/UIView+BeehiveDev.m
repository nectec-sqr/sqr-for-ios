//
//  UIView+BeehiveDev.m
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/26/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import "UIView+BeehiveDev.h"
#import "UIImage+BeehiveDev.h"

@implementation UIView (BeehiveDev)

- (void)applyBlurEffect{
    [self applyBlurEffect:[UIColor whiteColor]];
}

- (void)applyBlurEffect:(UIColor *)tintColor{
    CGRect screenFrame = [self screenFrame];
    UIImage* imageOfUnderlyingView = [self convertViewToImage];
    imageOfUnderlyingView = [imageOfUnderlyingView applyBlurWithRadius:30
                                                             tintColor:[tintColor colorWithAlphaComponent:0.3]
                                                 saturationDeltaFactor:1.4
                                                             maskImage:nil];
    
    self.backgroundColor = [UIColor clearColor];
    UIImageView* backView = [[UIImageView alloc] initWithFrame:screenFrame];
    backView.image = imageOfUnderlyingView;
    [self insertSubview:backView atIndex:0];
}

- (CGRect) screenFrame {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIViewController *viewController = keyWindow.rootViewController;
    CGRect screenRect = viewController.view.bounds;
    
    return screenRect;
}

- (UIImage *)convertViewToImage {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = keyWindow.bounds;
    
    UIImage *capturedScreen = nil;
    if ([keyWindow respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        UIGraphicsBeginImageContextWithOptions(rect.size, YES, 0.0f);
        [keyWindow drawViewHierarchyInRect:rect afterScreenUpdates:YES];
        capturedScreen = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }else {
        capturedScreen = [UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:0.4]];
    }
    
    return capturedScreen;
}

@end
