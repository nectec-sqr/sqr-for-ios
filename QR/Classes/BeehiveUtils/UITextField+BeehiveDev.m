//
//  UITextField+fm995.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 3/9/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import "UITextField+BeehiveDev.h"

@implementation UITextField (BeehiveDev)

- (BOOL)isEmpty{
    BOOL empty = (self.text.length == 0) || [self.text isEqualToString:@" "];
    if (empty) {
        [self becomeFirstResponder];
    }
    return empty;
}

@end
