//
//  Utils.h
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/27/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (BOOL)isOS7Later;
+ (NSString *) appVersion;
+ (NSString *) build;
+ (NSString *) versionBuild;

+ (BOOL) emailValidation:(NSString *)candidate;

@end
