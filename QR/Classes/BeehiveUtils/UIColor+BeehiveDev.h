//
//  UIColor+BeehiveDev.h
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/25/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

@interface UIColor (BeehiveDev)

@end
