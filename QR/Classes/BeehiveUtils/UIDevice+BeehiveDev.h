//
//  UIDevice+BeehiveDev.h
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/25/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (BeehiveDev)

+ (BOOL)isPad;
+ (BOOL)isIPhone4;
+ (BOOL)isIPhone5;
+ (BOOL)isIPhone6;
+ (BOOL)isIPhone6Plus;
- (BOOL)isMinimumOfVersion:(float)minVersion;

@end
