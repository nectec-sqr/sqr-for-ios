//
//  UIButton+BeehiveDev.m
//  hugburiram
//
//  Created by Kiettiphong Manovisut on 2/12/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import "UIButton+BeehiveDev.h"

@implementation UIButton (BeehiveDev)

- (void)centerImageAndTitle:(float)spacing{
    // get the size of the elements here for readability
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = [self.titleLabel.text sizeWithAttributes: @{NSFontAttributeName:self.titleLabel.font}];
    
    // get the height they will take up as a unit
    CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);
    
    // raise the image and push it right to center it
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height), 0.0, 0.0, - titleSize.width);
    
    // lower the text and push it left to center it
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, - (totalHeight - titleSize.height),0.0);
}

- (void)centerImageAndTitle{
    const int DEFAULT_SPACING = 6.0f;
    [self centerImageAndTitle:DEFAULT_SPACING];
}

- (void)startActivityIndicatorLoading{
    int size = 20;
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] init];
    loading.frame = CGRectMake(CGRectGetWidth(self.frame) - size - 10, (CGRectGetHeight(self.frame)/2) - (size/2), size, size);
    [loading startAnimating];
    [loading setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self addSubview:loading];
}

- (void)stopActivityIndicatorLoading{
    for (UIView *view in self.subviews) {
        if ([view class] == [UIActivityIndicatorView class]) {
            [view removeFromSuperview];
        }
    }
}

@end
