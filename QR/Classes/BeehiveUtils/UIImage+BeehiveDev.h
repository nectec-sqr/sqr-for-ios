//
//  UIImage+BeehiveDev.h
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/25/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (BeehiveDev)

+ (UIImage *)imageFromColor:(UIColor *)color frame:(CGRect)rect;
+ (UIImage *)imageWithColor:(UIColor *)color;

- (UIImage *)applyLightEffect;
- (UIImage *)applyExtraLightEffect;
- (UIImage *)applyDarkEffect;
- (UIImage *)applyTintEffectWithColor:(UIColor *)tintColor;

- (UIImage *)applyBlurWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;


@end
