//
//  UIAlertView+BeehiveDev.m
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/28/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import "UIAlertView+BeehiveDev.h"

@implementation UIAlertView (BeehiveDev)

+ (void)showMessage:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"ตกลง", nil) otherButtonTitles:nil];
    [alert show];
}

+ (void)showMessage:(NSString *)msg inView:(UIViewController *)view completion:(CompletionBlock)block{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ตกลง", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        block();
    }]];
    [view presentViewController:alert animated:YES completion:nil];
}

+ (void)showConfirmMessage:(NSString *)msg inView:(UIViewController *)view completion:(CompletionBlock)block{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ตกลง", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        block();
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ยกเลิก", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
 
    }]];
    [view presentViewController:alert animated:YES completion:nil];
}

+ (void)showConfirmMessage:(NSString *)msg inView:(UIViewController *)view completion:(CompletionBlock)block cancel:(CompletionBlock)cancel{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ตกลง", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        block();
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ยกเลิก", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        cancel();
    }]];
    [view presentViewController:alert animated:YES completion:nil];
}

+ (void)oops{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!", @"Oops!") message:NSLocalizedString(@"Something wrong", @"Something wrong") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
    [alert show];
}

@end
