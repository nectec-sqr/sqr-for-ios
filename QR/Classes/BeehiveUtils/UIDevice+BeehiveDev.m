//
//  UIDevice+BeehiveDev.m
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/25/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import "UIDevice+BeehiveDev.h"

@implementation UIDevice (BeehiveDev)

+ (BOOL)isPad {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

+ (BOOL)isIPhone4 {
    return ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [UIScreen mainScreen].scale == 2.0f && [[UIScreen mainScreen] bounds].size.height == 480);
}

+ (BOOL)isIPhone5 {
    return ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [UIScreen mainScreen].scale == 2.0f && [[UIScreen mainScreen] bounds].size.height == 568);
}

+ (BOOL)isIPhone6 {
    return ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [UIScreen mainScreen].scale == 2.0f && [[UIScreen mainScreen] bounds].size.height == 667);
}

+ (BOOL)isIPhone6Plus {
    return ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [UIScreen mainScreen].scale == 2.0f && [[UIScreen mainScreen] bounds].size.height == 1104);
}

- (BOOL)isMinimumOfVersion:(float)minVersion {
    return ([[self systemVersion] floatValue] >= minVersion);
}

@end
