//
//  UITextField+BeehiveDev.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 3/9/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (BeehiveDev)

- (BOOL)isEmpty;

@end
