//
//  UIAlertView+BeehiveDev.h
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/28/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (BeehiveDev)

typedef void (^CompletionBlock)(void);

+ (void)showMessage:(NSString *)msg;
+ (void)showMessage:(NSString *)msg inView:(UIViewController *)view completion:(CompletionBlock)block;
+ (void)showConfirmMessage:(NSString *)msg inView:(UIViewController *)view completion:(CompletionBlock)block;
+ (void)showConfirmMessage:(NSString *)msg inView:(UIViewController *)view completion:(CompletionBlock)block cancel:(CompletionBlock)cancel;
+ (void)oops;

@end
