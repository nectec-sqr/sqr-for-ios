//
//  UIUnderlineView.m
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/27/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import "UIUnderlineView.h"

@implementation UIUnderlineView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.5f);
    CGContextSetStrokeColorWithColor(context, [[UIColor whiteColor] colorWithAlphaComponent:0.9].CGColor);
    
    CGContextMoveToPoint(context, 0, self.frame.size.height);
    CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height);
    CGContextStrokePath(context);
}

@end
