//
//  UIButton+BeehiveDev.h
//  hugburiram
//
//  Created by Kiettiphong Manovisut on 2/12/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (BeehiveDev)

- (void)centerImageAndTitle:(float)space;
- (void)centerImageAndTitle;

- (void)startActivityIndicatorLoading;
- (void)stopActivityIndicatorLoading;

@end
