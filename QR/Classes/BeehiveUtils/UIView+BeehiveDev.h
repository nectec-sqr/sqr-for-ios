//
//  UIView+BeehiveDev.h
//  BeehiveDeveloper
//
//  Created by Kiettiphong Manovisut on 1/26/15.
//  Copyright (c) 2015 Beehive Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BeehiveDev)

- (void)applyBlurEffect;
- (void)applyBlurEffect:(UIColor *)tintColor;

@end
