//
//  BaseService.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 1/7/15.
//  Copyright (c) 2015 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "APIClient.h"

@interface BaseService : NSObject{
    APIClient *_apiClient; // inherit
}

@property (nonatomic, strong) APIClient *apiClient; // Getter Setter

@end
