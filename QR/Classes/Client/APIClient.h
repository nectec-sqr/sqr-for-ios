//
//  APIClient.h
//  TVIS
//
//  Created by Kiettiphong Manovisut on 11/11/14.
//  Copyright (c) 2014 SPTDEV. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPRequestOperation;

@class QRForm;

typedef void (^APIRequestSuccess)(AFHTTPRequestOperation *operation, id result);
typedef void (^APIRequestIdle)(AFHTTPRequestOperation *operation, NSError *error);
typedef void (^APICompletionBlock)(id result, NSError *error);

@interface APIClient : NSObject

- (NSOperation *)qrDecodeWithForm:(QRForm *)form completion:(APICompletionBlock)block;

@end
