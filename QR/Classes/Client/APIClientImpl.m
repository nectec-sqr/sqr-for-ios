//
//  APIClientJsonImpl.m
//  TVIS
//
//  Created by Kiettiphong Manovisut on 11/11/14.
//  Copyright (c) 2014 SPTDEV. All rights reserved.
//

#import "APIClientImpl.h"

#import "BhHTTPRequestOperationManager.h"
#import "BhHTTPRequestSerializer.h"
#import "AFHTTPRequestOperation.h"
#import "AFURLConnectionOperation.h"

#import "NSString+QR.h"

#import "QRForm.h"

#define HTTP_POST @"POST"
#define HTTP_GET @"GET"

@implementation APIClientImpl{
    NSOperationQueue *_queue;
}

- (id)init{
    self = [super init];
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
    }
    return self;
}

#pragma mark Parser Json Block

static inline APIRequestSuccess requestSuccess(NSOperationQueue *parserQueue, NSString *responseClassname, APICompletionBlock block){
    return ^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"ResponseObject(Success) %@",responseObject);
        if ([operation isCancelled]) return;
        
        NSBlockOperation *parserOperation = [[NSBlockOperation alloc] init];
        __weak NSBlockOperation *weakParserOperation = parserOperation;
        
        [parserOperation addExecutionBlock:^{
            if([weakParserOperation isCancelled]) return;
            
            id resultObject = [[NSClassFromString(responseClassname) alloc] initWithDictionary:responseObject];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (block) {
                    block(resultObject, nil);
                }
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            }];
        }];
        
        [parserQueue addOperation:parserOperation];
    };
}

#pragma mark Response Error

static inline APIRequestIdle requestIdle(APICompletionBlock block){
    return ^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"ResponseObject(Idle) %@", operation.responseString);
        if ([operation isCancelled]) return;
        
        NSError *resultError = error;
        NSLog(@"Error Domain: %@ (%ld)", error.domain, (long)operation.response.statusCode);
        
        if ([error.domain isEqualToString:AFURLResponseSerializationErrorDomain]) {
            NSInteger statusCode = operation.response.statusCode;
            
            if (statusCode == 400 /* user error */ ||
                statusCode == 404 /* not found */ ||
                statusCode == 500 /* technical error */ ||
                statusCode == 503 /* server maintenace */) {
                
                NSError *wnError = nil;
                
                if (wnError) {
                    resultError = wnError;
                } else {
                    NSLog(@"Client Error %ld: WNError not found.", (long)statusCode);
                }
            } else if (statusCode == 200) {
                // Check if the content type is not supported
                if (error.code == NSURLErrorCannotDecodeContentData) {
                    NSLog(@"%@", error.localizedDescription);
                    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithDictionary:resultError.userInfo];
                    [userInfo setValue:@"Unexpected content type" forKey:NSLocalizedDescriptionKey];
                    
                    resultError = [[NSError alloc] initWithDomain:error.domain
                                                             code:error.code
                                                         userInfo:userInfo];
                }
            }
        }
        
        if (block) {
            block(nil, resultError);
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    };
}

#pragma mark - RequestOperationManager

- (AFHTTPRequestOperation *)HTTPRequestOperationWithRequest:(NSURLRequest *)request
                                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    NSLog(@"%@?%@", request.URL, [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding]);
    return [_operationManager HTTPRequestOperationWithRequest:request success:success failure:failure];
}

#pragma mark - RequestSerializer

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(id)parameters{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    return [_serializer requestWithMethod:method
                                URLString:URLString
                               parameters:parameters
                                    error:nil];
}

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withPhotoUploads:(NSArray *)photosUpload
                                                   name:(NSString *)name
                                               fileName:(NSString *)fileName
                                               mimeType:(NSString *)mimeType{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    return [_serializer multipartFormRequestWithMethod:method
                                             URLString:URLString
                                            parameters:parameters
                                      withPhotoUploads:photosUpload
                                                  name:name
                                              fileName:fileName
                                              mimeType:mimeType
                                                 error:nil];
}

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withVideoUploads:(NSArray *)videosUpload
                                                   name:(NSString *)name
                                               fileName:(NSString *)fileName
                                               mimeType:(NSString *)mimeType
                                                  error:(NSError *__autoreleasing *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    return [_serializer multipartFormRequestWithMethod:method
                                             URLString:URLString
                                            parameters:parameters
                                      withVideoUploads:videosUpload
                                                  name:name
                                              fileName:fileName
                                              mimeType:mimeType
                                                 error:nil];
}

- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method
                                              URLString:(NSString *)URLString
                                             parameters:(NSDictionary *)parameters
                                       withPhotoUploads:(NSArray *)photosUpload
                                             photosName:(NSString *)photosName
                                         photosFileName:(NSString *)photosFileName
                                         photosMimeType:(NSString *)photosMimeType
                                           videosUpload:(NSArray *)videosUpload
                                             videosName:(NSString *)videosName
                                         videosFileName:(NSString *)videosFileName
                                         videosMimeType:(NSString *)videosMimeType
                                                  error:(NSError *__autoreleasing *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    return [_serializer multipartFormRequestWithMethod:method
                                             URLString:URLString
                                            parameters:parameters
                                      withPhotoUploads:photosUpload
                                            photosName:photosName
                                        photosFileName:photosFileName
                                        photosMimeType:photosMimeType
                                          videosUpload:videosUpload
                                            videosName:videosName
                                        videosFileName:videosFileName
                                        videosMimeType:videosMimeType
                                                 error:error];
}

#pragma mark - Default Params

- (NSMutableDictionary *)getDefaultDictionary{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:@"7VDhAhLpqLQtzp3T" forKey:@"appkey"];
    [params setValue:@"ios" forKey:@"src"];
    return params;
}

#pragma mark - Implement APIClient

#pragma mark - QRCode



- (NSOperation *)qrDecodeWithForm:(QRForm *)form completion:(APICompletionBlock)block{
    NSMutableDictionary *values = [[NSMutableDictionary alloc] init];
    [values setValue:@"user" forKey:@"controller"];
    [values setValue:@"login" forKey:@"action"];
    [values setValue:form.code forKey:@"code"];
    
    NSURLRequest *request = [self requestWithMethod:HTTP_POST
                                          URLString:[NSString apiURL]
                                         parameters:values];
    return [self HTTPRequestOperationWithRequest:request
                                         success:requestSuccess(_queue, @"LoginResponse", block)
                                         failure:requestIdle(block)];
}

@end
