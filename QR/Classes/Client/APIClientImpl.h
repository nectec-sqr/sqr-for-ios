//
//  APIClientImpl.h
//  TVIS
//
//  Created by Kiettiphong Manovisut on 11/11/14.
//  Copyright (c) 2014 SPTDEV. All rights reserved.
//

#import "APIClient.h"

@class BhHTTPRequestOperationManager;
@class BhHTTPRequestSerializer;

@interface APIClientImpl : APIClient

@property (nonatomic) BhHTTPRequestOperationManager *operationManager;
@property (nonatomic) BhHTTPRequestSerializer *serializer;

@end
