//
//  Config.h
//  disabledtools
//
//  Created by Kiettiphong Manovisut on 8/22/15.
//  Copyright (c) 2015 Speech Developer. All rights reserved.
//

#ifndef disabledtools_Config_h
#define disabledtools_Config_h

#define ENABLE_DEBUG_MODE 0

#if ENABLE_DEBUG_MODE || ENABLE_DEBUG_API || ENABLE_DEBUG_PROFILE_IMAGE
#warning _debug are enabled
#endif

#if ENABLE_DEBUG_MODE
#define NSLog(fmt, ...) NSLog(fmt, ##__VA_ARGS__)
#else
#define NSLog(fmt, ...)
#endif

#define LATITUDE                13.7639239
#define LONGITUDE               100.5397085

static NSString * const UserLocationDidUpdateUserLocation = @"UserLocationDidUpdateUserLocation";
static NSString * const UserLocationDidUpdateUserLocationGeocoder = @"UserLocationDidUpdateUserLocationGeocoder";

#endif
