//
//  BaseResponse.h
//  fm995
//
//  Created by Kiettiphong Manovisut on 10/16/14.
//  Copyright (c) 2014 SpeechDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FIELD_STATUS    @"status"
#define FIELD_MESSAGE   @"message"

#define IS_OK           @"OK"
#define IS_UNKNOWN_ERR  @"UNKNOWN_ERROR"
#define IS_FAIL         @"FAIL"

@interface BaseResponse : NSObject

@property (nonatomic) BOOL isOK;
@property (nonatomic) BOOL isEnd;
@property (nonatomic) NSString *message;

- (instancetype)initWithDictionary:(NSDictionary *)jsonObject;

@end
