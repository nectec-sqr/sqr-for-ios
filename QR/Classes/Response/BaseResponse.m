//
//  BaseResponse.m
//  fm995
//
//  Created by Kiettiphong Manovisut on 10/16/14.
//  Copyright (c) 2014 SpeechDeveloper. All rights reserved.
//

#import "BaseResponse.h"

@implementation BaseResponse

- (instancetype)initWithDictionary:(NSDictionary *)jsonObject{
    if (!jsonObject) {
        return nil;
    }
    
    self = [super init];
    if (self) {
        if ([[jsonObject valueForKey:FIELD_STATUS] isEqualToString:IS_OK]) {
            _isOK = YES;
            _message = ([jsonObject valueForKey:FIELD_MESSAGE]) ? [jsonObject valueForKey:FIELD_MESSAGE] : nil;
        }else if ([[jsonObject valueForKey:FIELD_STATUS] isEqualToString:IS_FAIL]) {
            _message = ([jsonObject valueForKey:FIELD_MESSAGE]) ? [jsonObject valueForKey:FIELD_MESSAGE] : nil;
        }
    }
    return self;
}

@end
