//
//  Enum.h
//  disabledtools
//
//  Created by Kiettiphong Manovisut on 8/25/15.
//  Copyright (c) 2015 Speech Developer. All rights reserved.
//

#ifndef disabledtools_Enum_h
#define disabledtools_Enum_h

typedef NS_ENUM(NSUInteger, QRContentType) {
    kQRContentTypeBook = 1,
    kQRContentTypeExternalLink = 2,
    kQRContentTypeVideo = 3
};

typedef NS_ENUM(NSUInteger, QROfflineContentType) {
    kQROfflineContentTypeNotAvailable = 1,
    kQROfflineContentTypeAvailable = 2,
    kQROfflineContentTypeKeeped = 3
};

#endif
