//
//  QRForm.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import "BaseForm.h"

@interface QRForm : BaseForm

@property (nonatomic) NSString *code;

@end
