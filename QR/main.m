//
//  main.m
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

// IoC
#import "ServiceLocatorImpl.h"

#import "WrapperBeanFactory.h"
#import "BeanFactoryImpl.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
        BeanFactory *beanFactoryImpl = nil;
        beanFactoryImpl = [[BeanFactoryImpl alloc] init];
        [[WrapperBeanFactory sharedInstance] setBeanFactory:beanFactoryImpl];
        
        ServiceLocator *serviceLocator = [[ServiceLocatorImpl alloc] init];
        [[ServiceLocator sharedInstance] registerLocator:serviceLocator];
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
