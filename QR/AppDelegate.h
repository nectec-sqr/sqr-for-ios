//
//  AppDelegate.h
//  QR
//
//  Created by Kiettiphong Manovisut on 4/28/16.
//  Copyright © 2016 NECTEC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

